﻿using System.Collections.Generic;

namespace CleanArchitecture.Core.Entities
{
    public class Department
    {
        public Department()
        {
            Employee = new List<Employee>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Employee> Employee { get; set; }
    }
}
