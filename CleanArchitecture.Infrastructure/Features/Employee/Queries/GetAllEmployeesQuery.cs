﻿using AutoMapper;
using CleanArchitecture.Infrastructure.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Features.Employee.Queries
{
    public class GetAllEmployeesQuery : IRequest<List<Response>>
    {
        public class GetAllEmployeesQueryHandler : IRequestHandler<GetAllEmployeesQuery, List<Response>>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;
            public GetAllEmployeesQueryHandler(ApplicationDbContext context,IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<List<Response>> Handle(GetAllEmployeesQuery request, CancellationToken cancellationToken)
            {
                var employeeList = await _context.Employees.Include(e => e.Department).ToListAsync();
                if (employeeList == null)
                {
                    return null;
                }

                return _mapper.Map<List<Response>>(employeeList);
            }
        }
    }

    public class Response
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public string DepartmentName { get; set; }
    }
}
