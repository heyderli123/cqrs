﻿using AutoMapper;
using CleanArchitecture.Infrastructure.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Features.Employee.Queries
{
    public class GetEmployeeByIdQuery : IRequest<Response>
    {
        public int Id { get; set; }

        public class GetProductByIdQueryHandler : IRequestHandler<GetEmployeeByIdQuery, Response>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;
            public GetProductByIdQueryHandler(ApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<Response> Handle(GetEmployeeByIdQuery query, CancellationToken cancellationToken)
            {
                var employee = await _context.Employees.Include(e => e.Department).Where(e => e.Id == query.Id).FirstOrDefaultAsync();
                if (employee == null) return null;
                return _mapper.Map<Response>(employee);
            }
        }
    }
}
