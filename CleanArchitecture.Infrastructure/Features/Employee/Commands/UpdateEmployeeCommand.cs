﻿using AutoMapper;
using CleanArchitecture.Infrastructure.Context;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Features.Employee.Commands
{
    public class UpdateEmployeeCommand : IRequest<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public int DepartmentId { get; set; }

        public class UpdateEmployeeCommandHandler : IRequestHandler<UpdateEmployeeCommand, int>
        {
            private readonly ApplicationDbContext _context;
            
            public UpdateEmployeeCommandHandler(ApplicationDbContext Context, IMapper mapper)
            {
                _context = Context;

            }
            public async Task<int> Handle(UpdateEmployeeCommand command, CancellationToken cancellationToken)
            {
                var employee = _context.Employees.Where(a => a.Id == command.Id).FirstOrDefault();
                if (employee == null)
                {
                    return default;
                }
                else
                {
                    employee.Name = command.Name;
                    employee.Surname = command.Surname;
                    employee.Address = command.Address;
                    employee.DepartmentId = command.DepartmentId;
                    _context.Employees.Update(employee);
                    return await _context.SaveChangesAsync();
                }
            }
        }

    }
}
