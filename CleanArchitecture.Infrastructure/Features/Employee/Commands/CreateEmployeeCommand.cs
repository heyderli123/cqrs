﻿using CleanArchitecture.Infrastructure.Context;
using E = CleanArchitecture.Core.Entities.Employee;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;

namespace CleanArchitecture.Infrastructure.Features.Employee.Commands
{
    public class CreateEmployeeCommand : IRequest<int>
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public int DepartmentId { get; set; }


        public class CreateEmployeeCommandHandler : IRequestHandler<CreateEmployeeCommand, int>
        {
            private readonly ApplicationDbContext _context;
            private readonly IMapper _mapper;

            public CreateEmployeeCommandHandler(ApplicationDbContext Context,IMapper mapper)
            {
                _context = Context;
                _mapper = mapper;
            }

            public async Task<int> Handle(CreateEmployeeCommand command, CancellationToken cancellationToken)
            {
                var employee = _mapper.Map<E>(command);
                _context.Employees.Add(employee);
                return await _context.SaveChangesAsync();
            }
        }
    }
}
