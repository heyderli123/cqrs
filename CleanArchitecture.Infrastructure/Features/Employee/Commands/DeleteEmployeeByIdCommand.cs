﻿using CleanArchitecture.Infrastructure.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CleanArchitecture.Infrastructure.Features.Employee.Commands
{
    public class DeleteEmployeeByIdCommand : IRequest<int>
    {
        public int Id { get; set; }

        public class DeleteEmployeeByIdCommandHandler : IRequestHandler<DeleteEmployeeByIdCommand, int>
        {
            private readonly ApplicationDbContext _context;
            public DeleteEmployeeByIdCommandHandler(ApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<int> Handle(DeleteEmployeeByIdCommand command, CancellationToken cancellationToken)
            {
                var employee = await _context.Employees.Where(e => e.Id == command.Id).FirstOrDefaultAsync();
                if (employee == null) return default;
                _context.Employees.Remove(employee);
                return await _context.SaveChangesAsync();
            }
        }
    }
}
