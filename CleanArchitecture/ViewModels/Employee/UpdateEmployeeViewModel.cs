﻿namespace CleanArchitecture.ViewModels.Employee
{
    public class UpdateEmployeeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }       
        public int DepartmentId { get; set; }
    }
}
