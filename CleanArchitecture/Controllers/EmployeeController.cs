﻿using AutoMapper;
using CleanArchitecture.Infrastructure.Features.Employee.Commands;
using CleanArchitecture.Infrastructure.Features.Employee.Queries;
using CleanArchitecture.ViewModels.Employee;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CleanArchitecture.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private IMediator _mediator;
        public IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
        private readonly IMapper _mapper;

        public EmployeeController(IMapper mapper)
        {
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CreateEmployeeViewModel viewModel)
        {
            var command = _mapper.Map<CreateEmployeeCommand>(viewModel);
            await Mediator.Send(command);

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var employees = await Mediator.Send(new GetAllEmployeesQuery());

            return Ok(_mapper.Map<List<EmployeeDetailsViewModel>>(employees));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var employee = await Mediator.Send(new GetEmployeeByIdQuery { Id = id });
            return Ok(_mapper.Map<EmployeeDetailsViewModel>(employee));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteEmployeeByIdCommand { Id = id }));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] UpdateEmployeeViewModel viewModel)
        {
            if (id != viewModel.Id)
            {
                return BadRequest();
            }
            var command = _mapper.Map<UpdateEmployeeCommand>(viewModel);

            await Mediator.Send(command);

            return Ok();
        }
    }
}
