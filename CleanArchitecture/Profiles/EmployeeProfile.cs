﻿using AutoMapper;
using CleanArchitecture.Core.Entities;
using CleanArchitecture.Infrastructure.Features.Employee.Commands;
using CleanArchitecture.Infrastructure.Features.Employee.Queries;
using CleanArchitecture.ViewModels.Employee;

namespace CleanArchitecture.Profiles
{
    public class EmployeeProfile : Profile
    {
        public EmployeeProfile()
        {
            CreateMap<CreateEmployeeViewModel, CreateEmployeeCommand>();
            CreateMap<Response, EmployeeDetailsViewModel>();
            CreateMap<UpdateEmployeeViewModel, UpdateEmployeeCommand>();
            CreateMap<Employee, Response>();
            CreateMap<CreateEmployeeCommand, Employee>();
      
        }
    }
}
